from django.test import TestCase, Client
from django.urls import resolve
from .views import landing

class UnitTest(TestCase):
	def test_url_is_exist(self):
		response = Client().get('/info/')
		self.assertEqual(response.status_code, 200)
# Create your tests here.
	def test_using_index(self):
		found = resolve('/info/')
		self.assertEqual(found.func, landing)

	def test_using_to_do_list_template(self):
		response = Client().get('/info/')
		self.assertTemplateUsed(response, 'info.html')