from django.urls import path
from . import views

app_name = 'infoku'

urlpatterns = 	[
	path('', views.landing),
]