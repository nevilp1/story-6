from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from . import views
from .models import Status
from .forms import StatusForm
from selenium import webdriver
from django.utils import timezone
from . import models
from selenium.webdriver.chrome.options import Options
import time
import unittest

class UnitTest(TestCase):
	def test_url_is_exist(self):
		response = Client().get('')
		self.assertEqual(response.status_code, 200)

	def test_using_to_do_list_template(self):
		response = Client().get('')
		self.assertTemplateUsed(response, 'home.html')

	def test_using_index(self):
		found = resolve('/')
		self.assertEqual(found.func, views.landing)

	def test_model_create(self):
		new_status = Status.objects.create(isi = 'Pada hari minggu...', tanggal=timezone.now())
		counting_all_available_status = Status.objects.all().count()
		self.assertEqual(counting_all_available_status,1)

	def test_form_is_post(self):
		response = Client().post('/', data={
			'isi' : 'Hello!',
			})
		self.assertEqual(response.status_code, 302)

	def test_is_valid(self):
		response = StatusForm(data={
			'isi' : 'Hello!',
			})
		self.assertTrue(response.is_valid())


class Functional_test(LiveServerTestCase):
	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('disable-gpu')
		self.browser  = webdriver.Chrome(executable_path='./chromedriver', chrome_options=chrome_options)
		super(Functional_test, self).setUp()

	def tearDown(self):
		self.browser.quit()
		super(Functional_test, self).tearDown()

	def test_functional(self):
		self.browser.get(self.live_server_url)
		idstatus = self.browser.find_element_by_id('id_isi')
		post = self.browser.find_element_by_name('post')

		idstatus.send_keys('baik')
		post.click()
		time.sleep(5)



# Create your tests here.
