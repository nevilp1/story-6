from django.db import models
from django.utils import timezone

class Status(models.Model):
	isi = models.CharField(max_length=300)
	tanggal = models.DateTimeField(default=timezone.now)

# Create your models here.
