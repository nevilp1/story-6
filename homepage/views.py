from django.shortcuts import render, redirect
from django.http import HttpResponse
from .forms import StatusForm
from .models import Status


# Create your views here.
def landing(request):
	if request.method =="POST":
		form = StatusForm(request.POST)
		if form.is_valid():
			form.save()
			return redirect('homepage:landingPage')
	else:
		form = StatusForm() 

	status = Status.objects.order_by("tanggal")
	listStatus = {
		'form' : form,
		'status' : status,
	}
	return render(request, "home.html", listStatus)