from . import models
from django import forms
import datetime

class StatusForm(forms.ModelForm):
    isi = forms.CharField(widget=forms.Textarea(attrs={
                "required" : True,
                "placeholder":"Apa yang ada dalam pikiranmu..",
                }))
  
   
    class Meta:
        model = models.Status
        fields = ["isi"]