from django.test import TestCase
from django.urls import resolve
from django.utils import timezone
from . import views
from django.http import HttpRequest

from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions
from selenium import webdriver

class UnitTest(TestCase):

    def test_url_exist(self):
        response = self.client.get('/story9/')
        self.assertEqual(response.status_code, 302)
        response = self.client.get('/story9/login')
        self.assertEqual(response.status_code, 200)
        response = self.client.get('/story9/logout')
        self.assertEqual(response.status_code, 302)

    def test__views_function(self):
        found = resolve('/story9/')
        self.assertEqual(found.func, views.index)
        found = resolve('/story9/login')
        self.assertEqual(found.func, views.login)

    def test_using_right_template(self):
        response = self.client.get('/story9/login')
        self.assertTemplateUsed(response, 'login.html')

class FunctionalTest(TestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(FunctionalTest, self).setUp()

        #set tearing down
    def tearDown(self):
        self.browser.quit()
        super(FunctionalTest, self).tearDown()

        #test opening the web
    def test_open(self):
        self.browser.get('http://localhost:8000/story9/')
        self.assertEqual(self.browser.title, "Story 9 | Login")
        self.assertIn("Login", self.browser.page_source)

    def test_login(self):
        self.browser.get('http://localhost:8000/story9/login')
        username = self.browser.find_element_by_id("id_username")
        username.send_keys("admin")
        password = self.browser.find_element_by_id("id_password")
        password.send_keys("admin")
        password.submit()
        self.assertIn("Hello admin", self.browser.page_source)
    

