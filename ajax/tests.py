from django.test import TestCase, Client
from django.urls import resolve
from . views import index

#selenium
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time

class UnitTest(TestCase):
	def test_url_is_exist(self):
		response = Client().get('/books/')
		self.assertEqual(response.status_code, 200)

	def test_using_index(self):
		found = resolve('/books/')
		self.assertEqual(found.func, index)

	def test_using_to_do_list_template(self):
		response = Client().get('/books/')
		self.assertTemplateUsed(response, 'index.html')

class Functional_test(TestCase):
	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('disable-gpu')
		self.browser  = webdriver.Chrome(executable_path='./chromedriver', chrome_options=chrome_options)
		super(Functional_test, self).setUp()

	def tearDown(self):
		self.browser.quit()
		super(Functional_test, self).tearDown()

	def test_success_append_div(self):
		self.browser.get('http://localhost:8000/books/')
		time.sleep(1)
		searchBox = self.browser.find_element_by_id('book-search')
		time.sleep(3)
		post = self.browser.find_element_by_id('b1')

		searchBox.send_keys('kalkulus')
		post.click()
		time.sleep(2)

		result = self.browser.find_elements_by_id('item')

		status = 0
		if(len(result) != 0):
			status = 200
		else:
			status = 404

		self.assertEqual(status, 200)



# Create your tests here.
