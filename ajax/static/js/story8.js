            $.ajax({
                method: 'GET',
                url: "https://www.googleapis.com/books/v1/volumes?q=ajax",
                success: function(hasil){
                    let book = $('#result');
                    book.empty();
                    const resultList = hasil.items;
                    for(var i = 0; i< resultList.length ; i++){
                        book.append(

                        "<div class='col-lg-4 col-sm-6 mb-4' id='item'>" + 
                        "<div class='card h-100' style='padding-left:30px'>" + 
                        "<img class='card-img-top' src="+ resultList[i].volumeInfo.imageLinks.thumbnail + ">" +
                        "<div class='card-body'>" + "<a href=" + resultList[i].volumeInfo.infoLink + ">" +
                        "<h5 class='card-title'>" + resultList[i].volumeInfo.title  + 
                        "</h5>" + "</a>" +
                        "<h6> Penulis: "+ resultList[i].volumeInfo.authors + "</h6>" +"</div>" + 
                        "</div>" + 
                        "</div>"
                        );
                    }

                }
            });




$(document).ready(
        $('#book-form').submit(function(event){
            event.preventDefault();
            const judul = $('#book-search').val();
            //alert(judul);

            $.ajax({
                method: 'GET',
                url: "https://www.googleapis.com/books/v1/volumes?q="+judul,
                success: function(hasil){
                    let book = $('#result');
                    book.empty();
                    const resultList = hasil.items;
                    book.append("<div class='container' style='margin-bottom:20px'> <h4> Search result for " + judul + "</h4> </div>");
                    for(var i = 0; i< resultList.length ; i++){
                        book.append(
                        "<div class='col-lg-4 col-sm-6 mb-4' id='item'>" + 
                        "<div class='card h-100' style='padding-left:30px'>" + 
                        "<img class='card-img-top' src="+ resultList[i].volumeInfo.imageLinks.thumbnail + ">" +
                        "<div class='card-body'>" + "<a href=" + resultList[i].volumeInfo.infoLink + ">" +
                        "<h5 class='card-title'>" + resultList[i].volumeInfo.title  + 
                        "</h5>" + "</a>" +
                        "<h6> Penulis: "+ resultList[i].volumeInfo.authors + "</h6>" +"</div>" + 
                        "</div>" + 
                        "</div>"
                        );
                    }

                }
            });






        }//function 1 
        ) //submit

        );